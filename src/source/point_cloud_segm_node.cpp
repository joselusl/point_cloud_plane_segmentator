



//I/O stream
//std::cout
#include <iostream>


//ROS
#include "ros/ros.h"



#include "point_cloud_segm.h"



using namespace std;






int main(int argc,char **argv)
{

    PointCloudSegmentator MyPointCloudSegmentator(argc,argv);
    MyPointCloudSegmentator.open();

    MyPointCloudSegmentator.run();

    return 0;
}

