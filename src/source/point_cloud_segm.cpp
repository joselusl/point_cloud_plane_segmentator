#include "point_cloud_segm.h"




PointCloudSegmentator::PointCloudSegmentator(int argc,char **argv)
{
    //Ros Init
    ros::init(argc, argv, ros::this_node::getName());
    ros::NodeHandle nh;

    // Image transport
    imageTransport=new image_transport::ImageTransport(nh);

    // Flags
    flagImageRgbReceived=false;
    flagInputPCReceived=false;

    return;
}

PointCloudSegmentator::~PointCloudSegmentator()
{
    delete imageTransport;

    return;
}


int PointCloudSegmentator::open()
{
    ros::NodeHandle nh;


    // Subscribers and publishers
    // Subs point cloud
    inputPCSub = nh.subscribe< pcl::PointCloud<pcl::PointXYZRGB> >("/fuseon/asus_xtion_pro_live_rgbd_camera/asus_xtion_pro_live/depth_registered/points", 1, &PointCloudSegmentator::inputPCCallback,this);
    // Subs rgb image
    imageRgbSubs = imageTransport->subscribe("/fuseon/asus_xtion_pro_live_rgbd_camera/asus_xtion_pro_live/rgb/image_color", 1, &PointCloudSegmentator::imageRgbCallback, this);
    //imageRgbSubs = imageTransport->subscribe("/fuseon/asus_xtion_pro_live_rgbd_camera/asus_xtion_pro_live/rgb/image_color", 1, &PointCloudSegmentator::imageRgbCallback, this);
    //imageRgbSubs = imageTransport->subscribe("/fuseon/asus_xtion_pro_live_rgbd_camera/asus_xtion_pro_live/depth_registered/hw_registered/image_rect", 1, &PointCloudSegmentator::imageRgbCallback, this);


    // Pub
    segmentedPCPub = nh.advertise< pcl::PointCloud<pcl::PointXYZRGB> >("segmentedPointCloud", 1);
    segmentedPlaneCoefficientsPub= nh.advertise<pcl_msgs::ModelCoefficients>("segmentedPlaneCoefficients", 1);

    return 0;
}



int PointCloudSegmentator::run()
{

    ros::spin();

    return 0;
}


void PointCloudSegmentator::readParameters()
{
    // Topic names
    //
    ros::param::param<std::string>("~image_rgb_topic_name", imageRgbTopicName, "camera/image_raw");
    std::cout<<"image_rgb_topic_name="<<imageRgbTopicName<<std::endl;

    return;
}


void PointCloudSegmentator::inputPCCallback(const pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr& msg)
{
    // Store msg
    inputPCMsg=msg;

    // Flag
    flagInputPCReceived=true;

    // Process
    process();

    // End
    return;
}


void PointCloudSegmentator::imageRgbCallback(const sensor_msgs::ImageConstPtr& msg)
{
    //Transform image message to Opencv to be processed
    try
    {
        cvImageRgb = cv_bridge::toCvCopy(msg,sensor_msgs::image_encodings::BGR8);
        //cvImageRgb = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::TYPE_32FC1);
    }
    catch (cv_bridge::Exception& e)
    {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }


    // Flag
    flagImageRgbReceived=true;

    // Process
    process();

    // End
    return;
}


int PointCloudSegmentator::process()
{
    // Check flags
    if(!flagImageRgbReceived || !flagInputPCReceived)
        return -1;

    // Check Headers -> Not working! TODO check
//    ros::Time timeStamp;
//    pcl_conversions::fromPCL(inputPCMsg->header.stamp, timeStamp);
//    std::cout<<"RGB Image stamp: "<<cvImageRgb->header.stamp.toNSec()<<std::endl;
//    std::cout<<"PCL stamp:       "<<timeStamp.toNSec()<<std::endl;
//    if(cvImageRgb->header.stamp != timeStamp)
//        return -2;


    //
    std::cout<<std::endl<<"Seq:"<<inputPCMsg->header.seq<<std::endl;


    // Get RGB Image in OpenCV format
    imageRgbMat=cvImageRgb->image;



    // IMAGE RGB Processing
    if(0)
    {
        // Update GUI Window
            cv::imshow("Image window", imageRgbMat);
            cv::waitKey(1);

        // Set up the detector with default parameters.
        cv::SimpleBlobDetector detector;

        // Detect blobs.
        std::vector<cv::KeyPoint> keypoints;
        detector.detect( imageRgbMat, keypoints);

        // Draw detected blobs as red circles.
        // DrawMatchesFlags::DRAW_RICH_KEYPOINTS flag ensures the size of the circle corresponds to the size of blob
        cv::Mat im_with_keypoints;
        cv::drawKeypoints( imageRgbMat, keypoints, im_with_keypoints, cv::Scalar(0,0,255), cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS );

        // Show blobs
        cv::imshow("keypoints", im_with_keypoints );
        cv::waitKey(1);
    }

std::cout<<"inputPCMsg->size="<<inputPCMsg->points.size()<<std::endl;


    // Preprocess the Point Cloud: Remove NaN

    ros::Time timeCountNansIn=ros::Time::now();

    pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointCloud(new pcl::PointCloud<pcl::PointXYZRGB>);

    std::vector<int> mapp;
    pcl::removeNaNFromPointCloud(*inputPCMsg, *pointCloud, mapp);



    //    pcl::ExtractIndices<pcl::PointXYZRGB> eifilter (true);
    //    eifilter.setInputCloud (msg);

    //    pcl::PointIndices::Ptr pointsToRemove (new pcl::PointIndices);


    // Count number of nans

    //     int numNans=0;
    //    BOOST_FOREACH (const pcl::PointXYZRGB& pt, pointCloud->points)
    //    {
    //        if(isnan(pt.x) || isnan(pt.y) || isnan(pt.z))
    //        {
    //            numNans++;

    //        }
    //    }

    ros::Time timeCountNansFin=ros::Time::now();
    //    std::cout<<" -num nans="<<numNans<<std::endl;

    //eifilter.setNegative (false);
    //eifilter.filterDirectly (cloud_in);

    std::cout<<"pointCloud->size="<<pointCloud->points.size()<<std::endl;

    std::cout<<"  timeRemoveNans="<<(timeCountNansFin-timeCountNansIn).toNSec()/1e6<<" ms"<<std::endl;






    // Preprocess the Point Cloud: Downsample the point cloud
    // http://www.pointclouds.org/documentation/tutorials/voxel_grid.php#voxelgrid
    ros::Time timeReducePCSizeIn=ros::Time::now();

    pcl::PointCloud<pcl::PointXYZRGB>::Ptr reducedPointCloud(new pcl::PointCloud<pcl::PointXYZRGB>);
    // Create the filtering object
    pcl::VoxelGrid<pcl::PointXYZRGB> voxelGrid;
    voxelGrid.setInputCloud (pointCloud);
    voxelGrid.setLeafSize (0.015f, 0.015f, 0.015f);
    voxelGrid.filter (*reducedPointCloud);

    ros::Time timeReducePCSizeFin=ros::Time::now();

std::cout<<"reducedPointCloud->size="<<reducedPointCloud->points.size()<<std::endl;

std::cout<<"  timeReducePCSize="<<(timeReducePCSizeFin-timeReducePCSizeIn).toNSec()/1e6<<" ms"<<std::endl;

      // Filter the point cloud: http://www.pointclouds.org/documentation/tutorials/statistical_outlier.php#statistical-outlier-removal
    //      pcl::StatisticalOutlierRemoval<pcl::PointXYZRGB> sor;
    //        sor.setInputCloud (reducedPointCloud);
    //        sor.setMeanK (50);
    //        sor.setStddevMulThresh (1.0);
    //        sor.filter (*reducedPointCloud);







    // TODO: http://www.pointclouds.org/documentation/tutorials/model_outlier_removal.php#model-outlier-removal


    // Coarse model by RANSAC
    ros::Time timeDoRansacIn=ros::Time::now();
    // Prepare ransac fitting
    //ros::Time timePrepRansacIn=ros::Time::now();
    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices);


    // Create the segmentation object
    pcl::SACSegmentation<pcl::PointXYZRGB> seg;

    // Optional
    seg.setOptimizeCoefficients(true);
    // Mandatory
    seg.setModelType(pcl::SACMODEL_PLANE);
    seg.setMethodType(pcl::SAC_RANSAC);
    seg.setDistanceThreshold(0.01);


    // Set indices -> To filter NaN ?


    //ros::Time timePrepRansacFin=ros::Time::now();
    //std::cout<<"  timePrepRansac="<<(timePrepRansacFin-timePrepRansacIn).toNSec()/1e6<<std::endl;


    // Project points: http://www.pointclouds.org/documentation/tutorials/project_inliers.php#project-inliers


    // Do the Ransac
    //ros::Time timeDoRansacIn=ros::Time::now();
    seg.setInputCloud (reducedPointCloud);
    seg.segment (*inliers, *coefficients);

    if (inliers->indices.size () == 0)
    {
      PCL_ERROR ("Could not estimate a planar model for the given dataset.");
      return 1;
    }
    ros::Time timeDoRansacFin=ros::Time::now();
    std::cout<<"  timeDoRansac="<<(timeDoRansacFin-timeDoRansacIn).toNSec()/1e6<<" ms"<<std::endl;

    //    std::cerr << " -Model coefficients (ax+by+cz+d=0): " << coefficients->values[0] << " "
    //                                        << coefficients->values[1] << " "
    //                                        << coefficients->values[2] << " "
    //                                        << coefficients->values[3] << std::endl;
    std::cout<<" -Model coefficients coarse (ax+by+cz+d=0): "<<coefficients->values[0] << " "
                                                       << coefficients->values[1] << " "
                                                       << coefficients->values[2] << " "
                                                       << coefficients->values[3]<<std::endl;


    // Correspondance grouping
    // http://www.pointclouds.org/documentation/tutorials/correspondence_grouping.php

    // Refine the model with the full point cloud
    // http://pointclouds.org/documentation/tutorials/model_outlier_removal.php
    // Calculate distance of the coarse model in the full point cloud
    // Extract the points with a small distance: candidates to inliers
    ros::Time timeExtractCandidatesIn=ros::Time::now();

    pcl::PointIndices::Ptr inliers_candidates (new pcl::PointIndices);
    double threshold_pc_fine=0.03;

    // EKF to refine
    Eigen::Array4f x;
    Eigen::Array3f z;

    Eigen::MatrixXf P(4,4);
    Eigen::MatrixXf Hx(3,4);

    Eigen::MatrixXf Q(4,4);
    Eigen::MatrixXf R(3,3);

    Eigen::MatrixXf S(3,3);
    Eigen::MatrixXf K(4,3);


    //double divisor=sqrt(pow(coefficients->values[0],2)+pow(coefficients->values[1],2)+pow(coefficients->values[2],2));
    for (size_t i = 0; i < pointCloud->points.size (); ++i)
    {
        //double distance=fabs(pointCloud->points[i].x*coefficients->values[0]+pointCloud->points[i].y*coefficients->values[1]+pointCloud->points[i].z*coefficients->values[2]+coefficients->values[3]);
        //distance/=divisor;
        double distance=pcl::pointToPlaneDistance(pointCloud->points[i],
                                                  coefficients->values[0], coefficients->values[1], coefficients->values[2], coefficients->values[3]);

        if(distance<threshold_pc_fine)
        {
            // This point is a candidate inlier
            inliers_candidates->indices.push_back(i);
        }
    }

ros::Time timeExtractCandidatesMid=ros::Time::now();

    // Get Point cloud
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr  cloud_candidates(new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl::ExtractIndices<pcl::PointXYZRGB> extract_candidates;

    extract_candidates.setInputCloud (pointCloud);
    extract_candidates.setIndices (inliers_candidates);

    extract_candidates.setNegative (false);
    extract_candidates.filter (*cloud_candidates);


ros::Time timeExtractCandidatesFin=ros::Time::now();
std::cout<<"  timeExtractCandidatesFirst="<<(timeExtractCandidatesMid-timeExtractCandidatesIn).toNSec()/1e6<<" ms"<<std::endl;
    std::cout<<"  timeExtractCandidates="<<(timeExtractCandidatesFin-timeExtractCandidatesIn).toNSec()/1e6<<" ms"<<std::endl;


std::cout<<"cloud_candidates->size="<<cloud_candidates->points.size()<<std::endl;



    // Refine the model with the candidates to inliers by RANSAC
// NO!! Update the model based in the previous pass!!
    ros::Time timeRefineRansacIn=ros::Time::now();

    pcl::ModelCoefficients::Ptr coefficients_fine (new pcl::ModelCoefficients);
    pcl::PointIndices::Ptr inliers_fine (new pcl::PointIndices);

    // Create the segmentation object
    pcl::SACSegmentation<pcl::PointXYZRGB> seg_fine;

    // Optional
    seg.setOptimizeCoefficients(true);
    // Mandatory
    seg_fine.setModelType(pcl::SACMODEL_PLANE);
    seg_fine.setMethodType(pcl::SAC_RANSAC);
    seg_fine.setDistanceThreshold(0.01);
    seg_fine.setMaxIterations(1);


    // Do the Ransac
    //ros::Time timeDoRansacIn=ros::Time::now();
    seg_fine.setInputCloud (cloud_candidates);
    seg_fine.segment (*inliers_fine, *coefficients_fine);

    if (inliers_fine->indices.size () == 0)
    {
      PCL_ERROR ("Could not estimate a planar model for the given dataset.");
      return 1;
    }


    ros::Time timeRefineRansacFin=ros::Time::now();
    std::cout<<"  timeRefineRansac="<<(timeRefineRansacFin-timeRefineRansacIn).toNSec()/1e6<<" ms"<<std::endl;

    std::cout<<" -Model coefficients fine (ax+by+cz+d=0): "<<coefficients_fine->values[0] << " "
                                                       << coefficients_fine->values[1] << " "
                                                       << coefficients_fine->values[2] << " "
                                                       << coefficients_fine->values[3]<<std::endl;


    // http://www.ditutor.com/distancias/punto_plano.html
    //double distanceToPlane=fabs(coefficients->values[3]) / sqrt(pow(coefficients->values[0],2)+pow(coefficients->values[1],2)+pow(coefficients->values[2],2));
    //double distanceToPlane=fabs(coefficients->values[3]);


    // Prepare Message for coefficients
    pcl_conversions::fromPCL(inputPCMsg->header.stamp, segmentedPlaneCoefficientsMsg.header.stamp);
    segmentedPlaneCoefficientsMsg.header.frame_id=inputPCMsg->header.frame_id;
    segmentedPlaneCoefficientsMsg.values=coefficients_fine->values;
    // Publish
    publishSegmentedPlaneCoefficient();


    //std::cerr <<"  -distance="<<distanceToPlane<<std::endl;



    //    std::cerr << "Model inliers: " << inliers->indices.size () << std::endl;
    //    for (size_t i = 0; i < inliers->indices.size (); ++i)
    //      std::cerr << inliers->indices[i] << "    " << cloud->points[inliers->indices[i]].x << " "
    //                                                 << cloud->points[inliers->indices[i]].y << " "
    //                                                 << cloud->points[inliers->indices[i]].z << std::endl;


    // Output point cloud
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr  cloud_p(new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl::ExtractIndices<pcl::PointXYZRGB> extract;
    if(0)
    {
        extract.setInputCloud (reducedPointCloud);
        extract.setIndices (inliers);

    }

    if(1)
    {
        extract.setInputCloud (cloud_candidates);
        extract.setIndices (inliers_fine);
    }
    extract.setNegative (false);
    extract.filter (*cloud_p);
std::cout<<"cloud_p->size="<<cloud_p->points.size()<<std::endl;

    // Publish point cloud
    publishSegmentedPC(cloud_p);

    //savePointCloudToFile("/home/joselusl/test_pcd.pcd",cloud_p);


    // Flags
    flagImageRgbReceived=false;
    flagInputPCReceived=false;

    return 0;

}


int PointCloudSegmentator::publishSegmentedPC(const pcl::PointCloud<pcl::PointXYZRGB>::Ptr& pointCloud)
{
    segmentedPCPub.publish(pointCloud);

    return 0;
}


int PointCloudSegmentator::publishSegmentedPlaneCoefficient()
{
    segmentedPlaneCoefficientsPub.publish(segmentedPlaneCoefficientsMsg);
    return 0;
}


int PointCloudSegmentator::printScreenPointCloud(const pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr& pointCloud)
{
    printf ("Cloud: width = %d, height = %d\n", pointCloud->width, pointCloud->height);

    BOOST_FOREACH (const pcl::PointXYZRGB& pt, pointCloud->points)
    {
        Eigen::Vector3i rgb=pt.getRGBVector3i();

        std::cout<<"Point: "<<pt.x<<", "<<pt.y<<", "<<pt.z<<"; "<<rgb[0]<<", "<<rgb[1]<<", "<<rgb[2]<<std::endl;

    }

    return 0;
}


int PointCloudSegmentator::savePointCloudToFile(std::string filePath, const pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr &pointCloud)
{
    pcl::io::savePCDFileASCII (filePath, *pointCloud);

    // To view: pcl_viewer /home/joselusl/test_pcd.pcd

    return 0;
}
