#ifndef POINT_CLOUD_SEGM_H
#define POINT_CLOUD_SEGM_H



//I/O stream
//std::cout
#include <iostream>

#include <cmath>




// Eigen
#include <eigen3/Eigen/Core>


// Boost
#include <boost/foreach.hpp>


// OpenCV
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/features2d/features2d.hpp"


// PCL
#include <pcl/ModelCoefficients.h>

#include <pcl/point_types.h>

#include <pcl/io/pcd_io.h>

#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>

#include <pcl/sample_consensus/sac_model_plane.h>

#include <pcl/segmentation/sac_segmentation.h>

#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/filter_indices.h>
#include <pcl/filters/filter.h>



// ROS
#include <ros/ros.h>


// PCL ROS
#include <pcl_conversions/pcl_conversions.h>

#include <pcl_ros/point_cloud.h>

#include <pcl_msgs/ModelCoefficients.h>


//ROS Images
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>

// Camera calibration
#include <sensor_msgs/CameraInfo.h>






class PointCloudSegmentator
{
public:
    PointCloudSegmentator(int argc,char **argv);
    ~PointCloudSegmentator();

public:
    int open();

    int run();

protected:
    void readParameters();


    // Point cloud
protected:
    bool flagInputPCReceived;
    pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr inputPCMsg; //TODO check
    ros::Subscriber inputPCSub;
    void inputPCCallback(const pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr& msg);


    // Images
protected:
    image_transport::ImageTransport* imageTransport;

    // RGB Images received
protected:
    bool flagImageRgbReceived;
    std::string imageRgbTopicName; // TODO
    cv_bridge::CvImagePtr cvImageRgb;
    cv::Mat imageRgbMat;
    //Subscriber
    image_transport::Subscriber imageRgbSubs;
    void imageRgbCallback(const sensor_msgs::ImageConstPtr& msg);



protected:
    int process();


protected:
    ros::Publisher segmentedPCPub;
    int publishSegmentedPC(const pcl::PointCloud<pcl::PointXYZRGB>::Ptr& pointCloud);

protected:
    ros::Publisher segmentedPlaneCoefficientsPub;
    pcl_msgs::ModelCoefficients segmentedPlaneCoefficientsMsg;
    int publishSegmentedPlaneCoefficient();


private:
    int printScreenPointCloud(const pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr &pointCloud);
    int savePointCloudToFile(std::string filePath, const pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr &pointCloud);

};



#endif
